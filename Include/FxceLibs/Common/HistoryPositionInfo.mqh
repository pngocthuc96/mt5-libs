//+------------------------------------------------------------------+
//|                                     CFxceHistoryPositionInfo.mqh |
//|                                 Copyright 2021, FXCE Company Ltd |
//|                                             https://www.fxce.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2021, FXCE Company Ltd"
#property link "https://www.fxce.com"
#include <Arrays/ArrayLong.mqh>
#include <Generic/HashSet.mqh>
#include <Trade/DealInfo.mqh>
#include <Trade/HistoryOrderInfo.mqh>

class CFxceHistoryPositionInfo : public CObject
{
protected:
   ulong m_curr_ticket; // ticket of closed position
   CArrayLong m_tickets;
   CDealInfo m_curr_deal;

public:
   CFxceHistoryPositionInfo(void);
   ~CFxceHistoryPositionInfo(void);

   ulong Ticket(void) const
   {
      return (m_curr_ticket);
   }
   // methods to the integer position properties
   datetime TimeOpen(void);
   ulong TimeOpenMsc(void);
   datetime TimeClose(void);
   ulong TimeCloseMsc(void);
   ENUM_DEAL_TYPE DealType(void);
   string TypeDescription(void);
   long Magic(void);
   long Identifier(void);
   ENUM_DEAL_REASON OpenReason(void);
   ENUM_DEAL_REASON CloseReason(void);

   // methods to the double position properties
   double Volume(void);
   double PriceOpen(void);
   double StopLoss(void) const;
   double TakeProfit(void) const;
   double PriceClose(void);
   double Commission(void);
   double Swap(void);
   double Profit(void);

   // methods to the string position properties
   string Symbol(void);
   string OpenComment(void);
   string CloseComment(void);
   string OpenReasonDescription(void);
   string CloseReasonDescription(void);
   string DealTickets(const string separator = " ");

   // info methods
   string FormatType(string &str, const uint type) const;
   string FormatReason(string &str, const uint reason) const;

   // methods for select position
   bool HistorySelect(datetime from_date, datetime to_date);
   int DealsTotal(void) const;
   bool SelectByTicket(const ulong ticket);
   bool SelectByIndex(const int index);

protected:
   bool HistoryPositionSelect(const long position_id) const;
   bool HistoryPositionCheck(const int log_level);
};

/**
 * @brief Construct a new CFxceHistoryPositionInfo::CFxceHistoryPositionInfo object
 * 
 */
CFxceHistoryPositionInfo::CFxceHistoryPositionInfo(void) : m_curr_ticket(0)
{
   ENUM_ACCOUNT_MARGIN_MODE margin_mode = (ENUM_ACCOUNT_MARGIN_MODE)AccountInfoInteger(ACCOUNT_MARGIN_MODE);
   if (margin_mode != ACCOUNT_MARGIN_MODE_RETAIL_HEDGING)
   {
      Print(" Error: no retail hedging!");
   }
}

/**
 * @brief Destroy the CFxceHistoryPositionInfo::CFxceHistoryPositionInfo object
 * 
 */
CFxceHistoryPositionInfo::~CFxceHistoryPositionInfo(void)
{
}

/**
 * @brief  Get the opening time of the selected position  
 * 
 * @return datetime 
 */
datetime CFxceHistoryPositionInfo::TimeOpen(void)
{
   datetime pos_time = 0;
   if (m_curr_ticket)
      if (m_curr_deal.SelectByIndex(0))
         pos_time = m_curr_deal.Time();
   return (pos_time);
}

/**
 * @brief Get the opening time of the selected position in milliseconds  
 * 
 * @return ulong Value of ulong type
 */
ulong CFxceHistoryPositionInfo::TimeOpenMsc(void)
{
   ulong pos_time_msc = 0;
   if (m_curr_ticket)
      if (m_curr_deal.SelectByIndex(0))
         pos_time_msc = m_curr_deal.TimeMsc();
   return (pos_time_msc);
}

/**
 * @brief Get the closing time of the selected position
 * 
 * @return datetime 
 */
datetime CFxceHistoryPositionInfo::TimeClose(void)
{
   datetime pos_time = 0;
   if (m_curr_ticket)
      if (m_curr_deal.SelectByIndex(HistoryDealsTotal() - 1))
         pos_time = m_curr_deal.Time();
   return (pos_time);
}

/**
 * @brief Get the closing time of the selected position in milliseconds 
 * 
 * @return ulong Value of ulong type
 */
ulong CFxceHistoryPositionInfo::TimeCloseMsc(void)
{
   ulong pos_time_msc = 0;
   if (m_curr_ticket)
      if (m_curr_deal.SelectByIndex(HistoryDealsTotal() - 1))
         pos_time_msc = m_curr_deal.TimeMsc();
   return (pos_time_msc);
}

/**
 * @brief Get the type of the selected position
 * 
 * @return ENUM_POSITION_TYPE 
 */
ENUM_DEAL_TYPE CFxceHistoryPositionInfo::DealType(void)
{
   ENUM_DEAL_TYPE pos_type = WRONG_VALUE;
   if (m_curr_ticket)
      if (m_curr_deal.SelectByIndex(0))
         pos_type = (ENUM_DEAL_TYPE)m_curr_deal.DealType();
   return (pos_type);
}

/**
 * @brief Get the type of the selected position as string 
 * 
 * @return string 
 */
string CFxceHistoryPositionInfo::TypeDescription(void)
{
   string str;
   return (FormatType(str, DealType()));
}

/**
 * @brief Get the magic number of the selected position 
 * 
 * @return long  Value of ulong type
 */
long CFxceHistoryPositionInfo::Magic(void)
{
   long pos_magic = WRONG_VALUE;
   if (m_curr_ticket)
      if (m_curr_deal.SelectByIndex(0))
         pos_magic = m_curr_deal.Magic();
   return (pos_magic);
}

/**
 * @brief  Get the identifier of the selected position
 * 
 * @return long  Value of ulong type
 */
long CFxceHistoryPositionInfo::Identifier(void)
{
   long pos_id = WRONG_VALUE;
   if (m_curr_ticket)
      if (m_curr_deal.SelectByIndex(0))
         pos_id = m_curr_deal.PositionId();
   return (pos_id);
}

/**
 * @brief Get the opening reason of the selected position
 * 
 * @return ENUM_DEAL_REASON 
 */
ENUM_DEAL_REASON CFxceHistoryPositionInfo::OpenReason(void)
{
   ENUM_DEAL_REASON pos_reason = WRONG_VALUE;
   long reason;
   if (m_curr_ticket)
      if (m_curr_deal.SelectByIndex(0))
         if (m_curr_deal.InfoInteger(DEAL_REASON, reason))
            pos_reason = (ENUM_DEAL_REASON)reason;
   return (pos_reason);
}

/**
 * @brief Get the closing reason of the selected position
 * 
 * @return ENUM_DEAL_REASON 
 */
ENUM_DEAL_REASON CFxceHistoryPositionInfo::CloseReason(void)
{
   ENUM_DEAL_REASON pos_reason = WRONG_VALUE;
   long reason;
   if (m_curr_ticket)
      if (m_curr_deal.SelectByIndex(HistoryDealsTotal() - 1))
         if (m_curr_deal.InfoInteger(DEAL_REASON, reason))
            pos_reason = (ENUM_DEAL_REASON)reason;
   return (pos_reason);
}

/**
 * @brief Get the volume of the selected position 
 * 
 * @return double  Value of double type
 */
double CFxceHistoryPositionInfo::Volume(void)
{
   double pos_volume = WRONG_VALUE;
   if (m_curr_ticket)
      if (m_curr_deal.SelectByIndex(0))
         pos_volume = m_curr_deal.Volume();
   return (pos_volume);
}

/**
 * @brief Get the opening price of the selected position
 * 
 * @return double Value of double type
 */
double CFxceHistoryPositionInfo::PriceOpen(void)
{
   double pos_price = WRONG_VALUE;
   if (m_curr_ticket)
      if (m_curr_deal.SelectByIndex(0))
         pos_price = m_curr_deal.Price();
   return (pos_price);
}

/**
 * @brief Get the Stop Loss price of the selected position 
 * 
 * @return double Value of double type
 */
double CFxceHistoryPositionInfo::StopLoss(void) const
{
   double pos_stoploss = WRONG_VALUE;
   long reason;
   CHistoryOrderInfo m_curr_order;
   if (m_curr_ticket)
      if (m_curr_order.SelectByIndex(HistoryOrdersTotal() - 1))
         if (m_curr_order.InfoInteger(ORDER_REASON, reason))
            if (reason == ORDER_REASON_SL)
               pos_stoploss = m_curr_order.PriceOpen();
            else if (m_curr_order.SelectByIndex(0))
               pos_stoploss = m_curr_order.StopLoss();
   return (pos_stoploss);
}

/**
 * @brief Get the Take Profit price of the selected position
 * 
 * @return double Value of double type
 */
double CFxceHistoryPositionInfo::TakeProfit(void) const
{
   double pos_takeprofit = WRONG_VALUE;
   long reason;
   CHistoryOrderInfo m_curr_order;
   if (m_curr_ticket)
      if (m_curr_order.SelectByIndex(HistoryOrdersTotal() - 1))
         if (m_curr_order.InfoInteger(ORDER_REASON, reason))
            if (reason == ORDER_REASON_TP)
               pos_takeprofit = m_curr_order.PriceOpen();
            else if (m_curr_order.SelectByIndex(0))
               pos_takeprofit = m_curr_order.TakeProfit();
   return (pos_takeprofit);
}

/**
 * @brief  Get the closing price of the selected position
 * 
 * @return double  Value of double type
 */
double CFxceHistoryPositionInfo::PriceClose(void)
{
   double pos_cprice = WRONG_VALUE;
   double sumVolTemp = 0;
   double sumMulTemp = 0;
   if (m_curr_ticket)
      for (int i = 0; i < HistoryDealsTotal(); i++)
         if (m_curr_deal.SelectByIndex(i))
            if (m_curr_deal.Entry() == DEAL_ENTRY_OUT || m_curr_deal.Entry() == DEAL_ENTRY_OUT_BY)
            {
               sumVolTemp += m_curr_deal.Volume();
               sumMulTemp += m_curr_deal.Price() * m_curr_deal.Volume();
               pos_cprice = sumMulTemp / sumVolTemp;
            }
   return (pos_cprice);
}

/**
 * @brief Get the amount of commission of the selected position  
 * 
 * @return double  Value of double type
 */
double CFxceHistoryPositionInfo::Commission(void)
{
   double pos_commission = 0;
   if (m_curr_ticket)
      for (int i = 0; i < HistoryDealsTotal(); i++)
         if (m_curr_deal.SelectByIndex(i))
            pos_commission += m_curr_deal.Commission();
   return (pos_commission);
}

/**
 * @brief Get the amount of swap of the selected position  
 * 
 * @return double Value of double type
 */
double CFxceHistoryPositionInfo::Swap(void)
{
   double pos_swap = 0;
   if (m_curr_ticket)
      for (int i = 0; i < HistoryDealsTotal(); i++)
         if (m_curr_deal.SelectByIndex(i))
            pos_swap += m_curr_deal.Swap();
   return (pos_swap);
}

/**
 * @brief Get the amount of profit of the selected position 
 * 
 * @return double Value of double type
 */
double CFxceHistoryPositionInfo::Profit(void)
{
   double pos_profit = 0;
   if (m_curr_ticket)
      for (int i = 0; i < HistoryDealsTotal(); i++)
         if (m_curr_deal.SelectByIndex(i))
            if (m_curr_deal.Entry() == DEAL_ENTRY_OUT || m_curr_deal.Entry() == DEAL_ENTRY_OUT_BY)
               pos_profit += m_curr_deal.Profit();
   return (pos_profit);
}

/**
 * @brief Get the symbol name of the selected position
 * 
 * @return string 
 */
string CFxceHistoryPositionInfo::Symbol(void)
{
   string pos_symbol = NULL;
   if (m_curr_ticket)
      if (m_curr_deal.SelectByIndex(0))
         pos_symbol = m_curr_deal.Symbol();
   return (pos_symbol);
}

/**
 * @brief Get the opening comment of the selected position 
 * 
 * @return string 
 */
string CFxceHistoryPositionInfo::OpenComment(void)
{
   string pos_comment = NULL;
   if (m_curr_ticket)
      if (m_curr_deal.SelectByIndex(0))
         pos_comment = m_curr_deal.Comment();
   return (pos_comment);
}

/**
 * @brief Get the closing comment of the selected position 
 * 
 * @return string 
 */
string CFxceHistoryPositionInfo::CloseComment(void)
{
   string pos_comment = NULL;
   if (m_curr_ticket)
      if (m_curr_deal.SelectByIndex(HistoryDealsTotal() - 1))
         pos_comment = m_curr_deal.Comment();
   return (pos_comment);
}

/**
 * @brief Get the opening reason of the selected position as string 
 * 
 * @return string 
 */
string CFxceHistoryPositionInfo::OpenReasonDescription(void)
{
   string str;
   return (FormatReason(str, OpenReason()));
}

/**
 * @brief Get the closing reason of the selected position as string
 * 
 * @return string 
 */
string CFxceHistoryPositionInfo::CloseReasonDescription(void)
{
   string str;
   return (FormatReason(str, CloseReason()));
}

/**
 * @brief Get all the deal tickets of the selected position as string 
 * 
 * @param separator 
 * @return string 
 */
string CFxceHistoryPositionInfo::DealTickets(const string separator = " ")
{
   string str_deals = "";
   if (m_curr_ticket)
      for (int i = 0; i < HistoryDealsTotal(); i++)
         if (m_curr_deal.SelectByIndex(i))
         {
            if (str_deals != "")
               str_deals += separator;
            str_deals += (string)m_curr_deal.Ticket();
         }
   return (str_deals);
}

/**
 * @brief Converts the position type to text 
 * 
 * @param str 
 * @param type 
 * @return string 
 */
string CFxceHistoryPositionInfo::FormatType(string &str, const uint type) const
{

   str = "";
   switch (type)
   {
   case POSITION_TYPE_BUY:
      str = "buy";
      break;
   case POSITION_TYPE_SELL:
      str = "sell";
      break;
   default:
      str = "unknown position type " + (string)type;
   }
   return (str);
}

/**
 * @brief Get the property value "DEAL_REASON" as string  
 * 
 * @param str 
 * @param reason 
 * @return string 
 */
string CFxceHistoryPositionInfo::FormatReason(string &str, const uint reason) const
{

   str = "";
   switch (reason)
   {
   case DEAL_REASON_CLIENT:
      str = "client";
      break;
   case DEAL_REASON_MOBILE:
      str = "mobile";
      break;
   case DEAL_REASON_WEB:
      str = "web";
      break;
   case DEAL_REASON_EXPERT:
      str = "expert";
      break;
   case DEAL_REASON_SL:
      str = "sl";
      break;
   case DEAL_REASON_TP:
      str = "tp";
      break;
   case DEAL_REASON_SO:
      str = "so";
      break;
   case DEAL_REASON_ROLLOVER:
      str = "rollover";
      break;
   case DEAL_REASON_VMARGIN:
      str = "vmargin";
      break;
   case DEAL_REASON_SPLIT:
      str = "split";
      break;
   default:
      str = "unknown reason " + (string)reason;
      break;
   }
   return (str);
}

/**
 * @brief Retrieve the history of closed positions for the specified period
 * 
 * @param from_date 
 * @param to_date 
 * @return true 
 * @return false 
 */
bool CFxceHistoryPositionInfo::HistorySelect(datetime from_date, datetime to_date)
{
   if (!::HistorySelect(from_date, to_date))
   {
      Print(" Error: HistorySelect -> false. Error Code: ", GetLastError());
      return (false);
   }

   // clear all cached position
   m_tickets.Shutdown();

   //define a hashset to collect position IDs.
   CHashSet<long> set_positions;
   long curr_pos_id;

   //collect position ids of history deals into the hashset.
   // handle the case when a position has multiple deals out.
   int deals = HistoryDealsTotal();
   //for(int i = 0; i < deals && !IsStopped(); i++)
   for (int i = deals - 1; i >= 0 && !IsStopped(); i--)
      if (m_curr_deal.SelectByIndex(i))
         //if(m_curr_deal.Entry()==DEAL_ENTRY_IN)
         if (m_curr_deal.Entry() == DEAL_ENTRY_OUT || m_curr_deal.Entry() == DEAL_ENTRY_OUT_BY)
            if (m_curr_deal.DealType() == DEAL_TYPE_BUY || m_curr_deal.DealType() == DEAL_TYPE_SELL)
               if ((curr_pos_id = m_curr_deal.PositionId()) > 0)
                  set_positions.Add(curr_pos_id);

   long arr_positions[];
   // copy the elements from the set to a compatible one-dimensional array
   set_positions.CopyTo(arr_positions, 0);
   ArraySetAsSeries(arr_positions, true);

   // filter out all the open or partially closed positions.
   // copy the remaining fully closed positions to the member array
   int positions = ArraySize(arr_positions);
   for (int i = 0; i < positions && !IsStopped(); i++)
      if ((curr_pos_id = arr_positions[i]) > 0)
         if (HistoryPositionSelect(curr_pos_id))
            if (HistoryPositionCheck(0))
               if (!m_tickets.Add(curr_pos_id))
               {
                  Print(" Error: failed to add position ticket #", curr_pos_id);
                  return (false);
               }
   return (true);
}

/**
 * @brief Return the number of closed positions for the specified period
 * 
 * @return int 
 */
int CFxceHistoryPositionInfo::DealsTotal(void) const
{
   return (m_tickets.Total());
}

/**
 * @brief Select position by its ticket or identifier for further operation
 * 
 * @param ticket 
 * @return true Return true in case successful, otherwise false.
 */
bool CFxceHistoryPositionInfo::SelectByTicket(const ulong ticket)
{
   if (HistoryPositionSelect(ticket))
   {
      if (HistoryPositionCheck(1))
      {
         m_curr_ticket = ticket;
         return (true);
      }
   }

   m_curr_ticket = 0;
   return (false);
}

/**
 * @brief Select position by its index in the list for further operation
 * 
 * @param index 
 * @return true Return true in case successful, otherwise false.
 * Note: The list of closed positions are ordered by closing time.
 */
bool CFxceHistoryPositionInfo::SelectByIndex(const int index)
{
   ulong curr_pos_ticket = m_tickets.At(index);
   if (curr_pos_ticket < LONG_MAX)
   {
      if (HistoryPositionSelect(curr_pos_ticket))
      {
         m_curr_ticket = curr_pos_ticket;
         return (true);
      }
   }
   else
      Print(" Error: the index of selection is out of range.");

   m_curr_ticket = 0;
   return (false);
}

/**
 * @brief Retrieve history of deals and orders for the specified position
 * 
 * @param position_id 
 * @return true Return true in case successful, otherwise false.
 */
bool CFxceHistoryPositionInfo::HistoryPositionSelect(const long position_id) const
{
   if (!HistorySelectByPosition(position_id))
   {
      Print(" Error: HistorySelectByPosition -> false. Error Code: ", GetLastError());
      return (false);
   }
   return (true);
}

/**
 * @brief check that the currently selected position is fully closed  
 * 
 * @param log_level 
 * @return true Return true in case successful, otherwise false.
 */
bool CFxceHistoryPositionInfo::HistoryPositionCheck(const int log_level)
{
   //check - surely has to be one IN and one or more OUT
   int deals = HistoryDealsTotal();
   if (deals < 2)
   {
      if (log_level > 0)
         Print(" Error: the selected position is still open.");
      return (false);
   }
   double pos_open_volume = 0;
   double pos_close_volume = 0;
   for (int j = 0; j < deals; j++)
   {
      if (m_curr_deal.SelectByIndex(j))
      {
         if (m_curr_deal.Entry() == DEAL_ENTRY_IN)
            pos_open_volume = m_curr_deal.Volume();
         else if (m_curr_deal.Entry() == DEAL_ENTRY_OUT || m_curr_deal.Entry() == DEAL_ENTRY_OUT_BY)
            pos_close_volume += m_curr_deal.Volume();
      }
      else
      {
         Print(" Error: failed to select deal at index #", j);
         return (false);
      }
   }
   // check - the total volume of IN minus OUT has to be equal to zero If a position is still open, it will not be displayed in the history.
   if (MathAbs(pos_open_volume - pos_close_volume) > 0.00001)
   {
      if (log_level > 0)
         Print(" Error: the selected position is not yet fully closed.");
      return (false);
   }
   return (true);
}
